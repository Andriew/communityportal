﻿using System;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using PortalSpolecznosciowy.FilterContext;
using PortalSpolecznosciowy.Models;
using PortalSpolecznosciowy.Repository;

namespace PortalSpolecznosciowy.Controllers
{
    public class MainPanelController : Controller
    {
        private PortalSpolecznosciowyLocalEntities DB = new PortalSpolecznosciowyLocalEntities();

        public int PageSize = 10;

        [SessionExistFilter]
        public ActionResult Index(int page = 1, int option = 1)
        {
            string sessionLogin = Session["Login"].ToString();
            int userId = DB.Users.Single(x => x.Login == sessionLogin).ID;
            ProfilAndWall PaW = new ProfilAndWall();

            PaW.user = DB.Users.Single(c => c.Login == sessionLogin);

            if (option == 2) //pokaz moje wpisy
            {
                PaW.articles = DB.Articles
                    //.Where(x => x.Users_ID == userId)
                    .Where(x => x.Users_ID == PaW.user.ID)
                    .OrderByDescending(art => art.Date)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize);

                PaW.PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = DB.Articles.Count()
                };
            }
            else if (option == 1) // pokaz wpisy wszystich innych
            {
                PaW.user = DB.Users.Single(c => c.ID == userId);

                PaW.articles = DB.Articles
                    //.Where(x => x.Users_ID == userId)
                    .Where(x => x.Users_ID != PaW.user.ID)
                    .OrderByDescending(art => art.Date)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize);

                PaW.PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = DB.Articles.Count()
                };
            }
            else if (option == 3) // pokaz wpisy i moje i wszystich innych
            {
                PaW.articles = DB.Articles
                    .OrderByDescending(art => art.Date)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize);

                PaW.PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = DB.Articles.Count()
                };
            }

            int myId = DB.Users.Single(x => x.Login == sessionLogin).ID;
            PaW.Contacts = DB.Contacts.Where(x => x.UserId1 == myId || x.UserId2 == myId).OrderBy(x => x.Active).ToList();

            TempData["option"] = option;
            return View(PaW);
        }

        [HttpPost]
        [SessionExistFilter]
        public ActionResult AddArticle(ProfilAndWall article)
        {
            string userLogin = Session["Login"].ToString();
            if (article.ArticleContent == null && article.File == null)
            {
                TempData["error"] = "Musisz coś wpisać lub dodać obrazek";
                return RedirectToAction("Index");
            }

            Articles art = new Articles
            {
                Users_ID = DB.Users.Single(x => x.Login == userLogin).ID,
                Content = article.ArticleContent,
                Date = DateTime.Now,
                Accessible = 1,
                Image = null
            };

            DB.Articles.Add(art);
            DB.SaveChanges();
            
            if (article.File != null && article.File.ContentLength > 0)
            {
                try
                {
                    string extension = Path.GetExtension(article.File.FileName).ToLower();
                    if (extension == ".jpg" || extension == ".jpeg" || extension == ".png")
                    {
                        Image image = Image.FromStream(article.File.InputStream, true, true);
                        string path = @"~/ArticleImages/" + art.ID + ".jpeg";
                        image.Save(Server.MapPath(path), System.Drawing.Imaging.ImageFormat.Jpeg);
                        art.Image = path;
                        DB.Articles.AddOrUpdate(art);
                        DB.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("To nie jest obrazek!");
                    }
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Błąd: " + ex.Message;
                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [SessionExistFilter]
        public ActionResult AddComment(ProfilAndWall paw, int articleId, int accessible)
        {
            Uri url = null;
            if (paw.user == null)
            {
                paw.user = new Users()
                {
                    Login = Session["Login"].ToString()
                };
                url = Request.UrlReferrer;
            }

            if (paw.CommentContent == null && paw.File == null)
            {
                TempData["error"] = "Musisz napisać komentarz lub dodać obrazek";
                return RedirectToAction("Index");
            }

            Comments com = new Comments
            {
                Users_ID = DB.Users.Single(x => x.Login == paw.user.Login).ID,
                Article_ID = articleId,
                Date = DateTime.Now,
                Accessible = accessible,
                Content = paw.CommentContent,
                Image = null,
            };

            DB.Comments.Add(com);
            DB.SaveChanges();

            if (paw.File != null && paw.File.ContentLength > 0)
            {
                try
                {
                    string extension = Path.GetExtension(paw.File.FileName).ToLower();
                    if (extension == ".jpg" || extension == ".jpeg" || extension == ".png")
                    {
                        Image image = Image.FromStream(paw.File.InputStream, true, true);
                        string path = @"~/CommentImages/" + com.ID + ".jpeg";
                        image.Save(Server.MapPath(path), System.Drawing.Imaging.ImageFormat.Jpeg);
                        com.Image = path;
                        DB.Comments.AddOrUpdate(com);
                        DB.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("To nie jest obrazek!");
                    }
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Błąd: " + ex.Message;
                    return RedirectToAction("Index");
                }
            }

            if(url != null)
                return Redirect(url.ToString());
            else
                return RedirectToAction("Index");
        }

        [SessionExistFilter]
        public ActionResult EditProfile()
        {
            var userLogin = Session["Login"].ToString();

            UserEdit userEdit = new UserEdit
            {
                UserLogin = userLogin,
                ProfileImageUrl = DB.Users.Single(x => x.Login == userLogin).Profil_image
            };

            TempData["UserEmail"] = DB.Users.Single(x => x.Login == userLogin).Email;

            return View(userEdit);
        }
        
    }
}