﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PortalSpolecznosciowy.FilterContext;
using PortalSpolecznosciowy.Repository;

namespace PortalSpolecznosciowy.Controllers
{
    [Admin]
    [SessionExistFilter]
    public class AdminPanelController : Controller
    {
        private PortalSpolecznosciowyLocalEntities DB = new PortalSpolecznosciowyLocalEntities();

        public ActionResult Index()
        {
            IEnumerable<Users> users = DB.Users.ToList();
            return View(users);
        }

        public ActionResult ChangeRole(string userLogin, int role)
        {
            Users user = DB.Users.Single(x => x.Login == userLogin);
            user.Role = role;
            DB.SaveChanges();
            TempData["Message"] = "uprawnienia zmienione dla użytkownika " + userLogin;
            return RedirectToAction("Index");
        }

        public ActionResult BanUser(string userLogin)
        {
            Users user = DB.Users.Single(x => x.Login == userLogin);
            user.Banned = true;
            DB.SaveChanges();
            TempData["Message"] = "Użytkownik " + userLogin + " został zbanowany!";
            return RedirectToAction("Index");
        }

        public ActionResult UnBanUser(string userLogin)
        {
            Users user = DB.Users.Single(x => x.Login == userLogin);
            user.Banned = false;
            DB.SaveChanges();
            TempData["Message"] = "Użytkownik " + userLogin + " został odbanowany!";
            return RedirectToAction("Index");
        }

        public ActionResult DeleteUser(string userLogin)
        {
            Users user = DB.Users.Single(x => x.Login == userLogin);

            List<Contacts> contactsList = DB.Contacts.Where(x => x.UserId1 == user.ID || x.UserId2 == user.ID).ToList();
            DB.Contacts.RemoveRange(contactsList);
            
            List<Comments> commnetsList = DB.Comments.Where(x => x.Users_ID == user.ID).ToList();
            DB.Comments.RemoveRange(commnetsList);
            
            foreach (var comment in commnetsList)
            {
                string pathToFile = Server.MapPath(comment.Image);
                System.IO.File.Delete(pathToFile);
            }

            List<Articles> articlesesList = DB.Articles.Where(x => x.Users_ID == user.ID).ToList();
            DB.Articles.RemoveRange(articlesesList);

            foreach (var article in articlesesList)
            {
                string pathToFile = Server.MapPath(article.Image);
                System.IO.File.Delete(pathToFile);
            }

            DB.Users.Remove(user);
            DB.SaveChanges();

            TempData["Message"] = "Użytkownik " + userLogin + " został usunięty!";
            return RedirectToAction("Index");
        }


    }
}