﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PortalSpolecznosciowy.FuncHelpers;
using PortalSpolecznosciowy.Models;
using PortalSpolecznosciowy.Repository;

namespace PortalSpolecznosciowy.Controllers
{
    public class ForgottenPasswordController : Controller
    {
        private PortalSpolecznosciowyLocalEntities DB = new PortalSpolecznosciowyLocalEntities();

        public ActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult SendLink(string Email)
        {
            if ( DB.Users.Any(x => x.Email == Email) )
            {
                Users user = DB.Users.Single(x => x.Email == Email);
                TokenGenerator tg = new TokenGenerator();
                string token = tg.GenerateToken();
                user.Token = token;
                DB.SaveChanges();

                SendEmail se = new SendEmail();
                string server = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                se.SendForForgottenPassword(user, server);
                return View();
            }
            else
            {
                TempData["error"] = "Podany email nie istnieje w naszej bazie danych";
                return RedirectToAction("Index");
            }   
        }

        public ActionResult Activation()
        {
            string userLogin = HttpContext.Request.Params.Get("user");
            string token = HttpContext.Request.Params.Get("token");

            TokenGenerator tg = new TokenGenerator();
            string Token = tg.GenerateToken();

            if (userLogin == null || token == null)
            {
                TempData["error"] = "Podany link jest nieprawidłowy";
                return RedirectToAction("Index");
            }
            else
            if ( !(DB.Users.Any(u => u.Login == userLogin && u.Token == token) && tg.IsActual(token, 15)) )
            {
                TempData["error"] = "Podany link jest nieprawidłowy lub wygasł.";
                return RedirectToAction("Index");
            }

            ChangePassword changePasswrod = new ChangePassword();

            changePasswrod.Login = userLogin;
            changePasswrod.Ip = Request.UserHostAddress;
            changePasswrod.Token = Token;

            return View(changePasswrod);
        }
        
        public ActionResult GenerateToken(string Login)
        {
            TokenGenerator tg = new TokenGenerator();
            var token = tg.GenerateToken();
            try
            {
                Users userResponse = DB.Users.Single(x => x.Login == Login);
                userResponse.Token = token;
                DB.Users.AddOrUpdate(userResponse);
                DB.SaveChanges();
                var ip = Request.UserHostAddress;
                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { Success = true, token = token, Ip = ip });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, token = "error" });
            }
        }

        public ActionResult ChangePassword(ChangePassword changePassword)
        {
            var user = DB.Users.Single(x => x.Login == changePassword.Login);
            user.Password = changePassword.HashPassword;
            DB.SaveChanges();

            return View();
        }

    }
}