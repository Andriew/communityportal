﻿using System;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using PortalSpolecznosciowy.FilterContext;
using PortalSpolecznosciowy.Models;
using PortalSpolecznosciowy.Repository;

namespace PortalSpolecznosciowy.Controllers
{
    public class ArticleController : Controller
    {
        private PortalSpolecznosciowyLocalEntities DB = new PortalSpolecznosciowyLocalEntities();

        // GET: Article
        public ActionResult Index(int? articleId)
        {
            if (Session["Login"] != null && articleId != null)
            {
                string userLogin = Session["Login"].ToString();
                ArticleAndComments AaC = new ArticleAndComments
                {
                    Article = DB.Articles.Single(x => x.ID == articleId),
                    Comments = DB.Comments.Where(x => x.Article_ID == articleId).OrderBy(x => x.Date),
                    UserLogin = userLogin
                };
                return View(AaC);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult AddComment(ArticleAndComments aac)
        {
            if (aac.CommentContent == null && aac.File == null)
            {
                TempData["error"] = "Musisz napisać komentarz lub dodać obrazek";
                return RedirectToAction("Index", new { articleId = aac.ArticleID} );
            }

            Comments com = new Comments
            {
                Users_ID = DB.Users.Single(x => x.Login == aac.UserLogin).ID,
                Article_ID = aac.ArticleID,
                Date = DateTime.Now,
                Accessible = DB.Articles.Single(x => x.ID == aac.ArticleID).Accessible,
                Content = aac.CommentContent,
                Image = null,
            };

            DB.Comments.Add(com);
            DB.SaveChanges();

            if (aac.File != null && aac.File.ContentLength > 0)
            {
                try
                {
                    string extension = Path.GetExtension(aac.File.FileName).ToLower();
                    if (extension == ".jpg" || extension == ".jpeg" || extension == ".png")
                    {
                        Image image = Image.FromStream(aac.File.InputStream, true, true);
                        string path = @"~/CommentImages/" + com.ID + ".jpeg";
                        image.Save(Server.MapPath(path), System.Drawing.Imaging.ImageFormat.Jpeg);
                        com.Image = path;
                        DB.Comments.AddOrUpdate(com);
                        DB.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("To nie jest obrazek!");
                    }
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Błąd: " + ex.Message;
                    return RedirectToAction("Index");
                }
            }
            
            return RedirectToAction("Index", new { articleId = com.Article_ID });
        }

        [SessionExistFilter]
        public ActionResult EditArticle(int artId)
        {
            Articles art = DB.Articles.Single(x => x.ID == artId);
            EditArticle editArt = new EditArticle
            {
                ID = art.ID,
                Content = art.Content,
                Users_ID = art.Users_ID,
                Date = art.Date,
                Accessible = art.Accessible,
                ImagePath = art.Image
            };
            return View(editArt);
        }

        [SessionExistFilter]
        public ActionResult SaveEditArticle(EditArticle artModel)
        {
            Articles art = DB.Articles.Single(x => x.ID == artModel.ID);
            art.Content = artModel.Content;
            art.Date = DateTime.Now;
            if (artModel.Image)
            {
                string pathToFile = Server.MapPath(art.Image);
                System.IO.File.Delete(pathToFile);
                art.Image = String.Empty;
            }

            DB.SaveChanges();
            return RedirectToAction("Index", new { articleId = artModel.ID });
        }

        [SessionExistFilter]
        public ActionResult DeleteArticle(int artId, int authorId)
        {
            Articles art = DB.Articles.Single(x => x.ID == artId);

            if (!String.IsNullOrEmpty(art.Image))
            {
                string pathToFile = Server.MapPath(art.Image);
                System.IO.File.Delete(pathToFile);
            }

            DB.Articles.Remove(art);
            DB.SaveChanges();
            return RedirectToAction("UserProfile", "Users", new {userLogin = new GetUser(authorId).UserLogin });
        }

        [SessionExistFilter]
        public ActionResult DeleteComment(int artId, int comId)
        {
            Comments comment = DB.Comments.Single(x => x.ID == comId);

            if (!String.IsNullOrEmpty(comment.Image))
            {
                string pathToFile = Server.MapPath(comment.Image);
                System.IO.File.Delete(pathToFile);
            }
                
            DB.Comments.Remove(comment);
            DB.SaveChanges();
            return RedirectToAction("Index", new { articleId = artId });
        }

    }
}