﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PortalSpolecznosciowy.FilterContext;
using PortalSpolecznosciowy.Models;
using PortalSpolecznosciowy.Repository;

namespace PortalSpolecznosciowy.Controllers
{
    public class UsersController : Controller
    {
        private PortalSpolecznosciowyLocalEntities DB = new PortalSpolecznosciowyLocalEntities();
        private int PageSize = 10;

        public ActionResult Index()
        {
            List<Users> userList = DB.Users.ToList();
            return View(userList);
        }

        public ActionResult UserProfile(int page = 1)
        {
            string userLogin = HttpContext.Request.Params.Get("userLogin");
            if (string.IsNullOrEmpty(userLogin))
            {
                return RedirectToAction("Index");
            }
            try
            {
                int myId = 0;
                if (Session["Login"] != null)
                {
                    string myLogin = Session["Login"].ToString();
                    myId = DB.Users.Single(x => x.Login == myLogin).ID;
                }

                int friendId = DB.Users.Single(x => x.Login == userLogin).ID;

                bool existFriend = DB.Contacts.Any(x =>
                    (x.UserId1 == myId || x.UserId2 == myId) &&
                    (x.UserId1 == friendId || x.UserId2 == friendId));

                byte friendStatus = 0;

                if (existFriend && (myId != friendId))
                {
                    friendStatus = DB.Contacts.Single(x =>
                        (x.UserId1 == myId || x.UserId2 == myId) &&
                        (x.UserId1 == friendId || x.UserId2 == friendId)).Active;
                }

                UserProfile up = new UserProfile
                {
                    UserLogin = userLogin,
                    ProfileImageUrl = DB.Users.Single(x => x.Login == userLogin).Profil_image,
                    Role = DB.Users.Single(x => x.Login == userLogin).Role,

                    ArticleList = DB.Articles
                        .Where(x => x.Users.Login == userLogin)
                        .OrderByDescending(art => art.Date)
                        .Skip((page - 1)*PageSize)
                        .Take(PageSize)
                        .ToList(),

                    FriendStatus = friendStatus,

                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = page,
                        ItemsPerPage = PageSize,
                        TotalItems = DB.Articles.Count(x => x.Users.Login == userLogin)
                    },

                    UserContacts =
                        DB.Contacts.Where(x => x.UserId1 == friendId || x.UserId2 == friendId)
                            .OrderBy(x => x.Active)
                            .ToList()
                };

                return View(up);
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult GetUsers(string query)
        {
            var results = from N in DB.Users
                          where N.Login.StartsWith(query)
                          select new { N.Login };
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [SessionExistFilter]
        public ActionResult AddContact(string friendLogin)
        {
            string myLogin = Session["Login"].ToString();

            Contacts contact = new Contacts
            {
                UserId1 = DB.Users.Single(x => x.Login == myLogin).ID,
                UserId2 = DB.Users.Single(x => x.Login == friendLogin).ID,
                Active = 1
            };

            DB.Contacts.Add(contact);
            DB.SaveChanges();
            return RedirectToAction("UserProfile", new { userLogin = friendLogin });
        }

        [SessionExistFilter]
        public ActionResult ChangeRole( UserProfile ur )
        {
            Users user = DB.Users.Single(x => x.Login == ur.UserLogin);
            user.Role = ur.Role;
            DB.SaveChanges();
            return RedirectToAction("UserProfile", new { userLogin = ur.UserLogin});
        }

    }
}