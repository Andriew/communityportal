﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PortalSpolecznosciowy.FilterContext;
using PortalSpolecznosciowy.Repository;

namespace PortalSpolecznosciowy.Controllers
{
    public class ContactsController : Controller
    {

        private PortalSpolecznosciowyLocalEntities DB = new PortalSpolecznosciowyLocalEntities();

        [SessionExistFilter]
        public ActionResult Index()
        {
            IEnumerable<Contacts> contactList = DB.Contacts.OrderByDescending(x => x.Active).ToList();
            return View(contactList);
        }

        [SessionExistFilter]
        public ActionResult AcceptReqest(int id)
        {
            Contacts contact = DB.Contacts.Single(x => x.Id == id);
            contact.Active = 2;
            DB.SaveChanges();
            return RedirectToAction("Index", "MainPanel");
        }

        [SessionExistFilter]
        public ActionResult DeclineReqest(int id)
        {
            
            Contacts contact = DB.Contacts.Single(x => x.Id == id);
            DB.Contacts.Remove(contact);
            DB.SaveChanges();
            
            /*
            Contacts contact = DB.Contacts.Single(x => x.Id == id);
            contact.Active = 0;
            DB.SaveChanges();
            */
            return RedirectToAction("Index", "MainPanel");
        }

        [SessionExistFilter]
        public ActionResult DeleteContact(int id)
        {
            
            Contacts contact = DB.Contacts.Single(x => x.Id == id);
            DB.Contacts.Remove(contact);
            DB.SaveChanges();
            
            /*
            Contacts contact = DB.Contacts.Single(x => x.Id == id);
            contact.Active = 0;
            DB.SaveChanges();
            */
            return RedirectToAction("Index", "MainPanel");
        }

    }
}