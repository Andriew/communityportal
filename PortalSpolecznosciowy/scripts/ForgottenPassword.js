﻿$(function () {
    $('#log').click(function (e) {
        $.ajax({
            type: "POST",
            url: "/ForgottenPassword/GenerateToken",
            data: { Login: $("#Login").val() },
            success: function (token) {
                var validator = $("form").validate();
                if (token.token !== "error") {
                    var passhash = $.sha256($("#Password").val());
                    $("#HashPassword").val(passhash);
                    $("#Ip").val(token.Ip);
                    $("#Token").val(token.token);
                    $("form").submit();
                } else {
                    validator.showErrors({ Login: "Nie istnieje taki użytkownik" });
                }
            },
            error: function (a, b, c) {
                console.log(a);
                console.log(b);
                console.log(c);
            }
        });
    });
});