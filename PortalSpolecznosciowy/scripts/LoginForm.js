﻿$(function () {
    $('#log').click(function (e) {
        var validator = $("form").validate();
        if ($("#Password").val() == null || $("#Password").val() == "") {
            validator.showErrors({ Password: "Pole nie może być puste" });
        } else {
            $.ajax({
                type: "POST",
                url: "/Login/LoginUser",
                data: { login: $("#Login").val() },
                success: function (token) {
                    if (token.token !== "error") {
                        var passhash = $.sha256($("#Password").val());
                        var con = passhash + token.token;
                        var hash = $.sha256(con);
                        $("#PassTokenHash").val(hash);
                        $("#Ip").val(token.Ip);
                        $("#Password").val("");
                        $("form").submit();
                    } else {
                        validator.showErrors({ Login: "Nie istnieje taki użytkownik" });
                    }
                },
                error: function (a, b, c) {
                    console.log(a);
                    console.log(b);
                    console.log(c);
                }
            });
        }
    });
});

$(function ($) {
    $("#Password").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            document.getElementById('log').click();
        }
    });
});