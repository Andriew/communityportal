﻿using System.ComponentModel.DataAnnotations;

namespace PortalSpolecznosciowy.Models
{
    public class ChangePassword
    {
        public string Login { set; get; }

        [Required]
        [Display(Name = "Hasło")]
        [RegularExpression(@"^[a-zA-Z0-9_''-'\s]{8,40}$", ErrorMessage = "Podane znaki są niedozwolone lub hasło jest za krótkie")]
        public string Password { set; get; }
        
        [Required]
        [Display(Name = "Hasło")]
        [RegularExpression(@"^[a-zA-Z0-9_''-'\s]{8,40}$", ErrorMessage = "Podane znaki są niedozwolone lub hasło jest za krótkie")]
        [Compare("Password")]
        public string ConfirmPassword { set; get; }

        public string Token { set; get; }

        public string HashPassword { set; get; }

        public string Ip { set; get; }

    }
}
