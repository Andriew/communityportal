﻿using System;

namespace PortalSpolecznosciowy.Models
{
    public class PagingInfo
    {
        public int TotalItems { set; get; }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }

        public int TotalPages
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage); }
        }
    }
}
