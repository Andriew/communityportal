﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PortalSpolecznosciowy.Models
{
    public class RegisterUserView 
    {       
        [Required]
        [Key]
        [Display(Name = "Login")]
        [RegularExpression(@"^[a-zA-Z0-9_''-'\s]{3,40}$", ErrorMessage = "Podane znaki są niedozwolone lub login jest za krótki")]
        public string Login { set; get; }

        [Required]
        [Display(Name = "Hasło")]
        [RegularExpression(@"^[a-zA-Z0-9_''-'\s]{8,40}$", ErrorMessage = "Podane znaki są niedozwolone lub hasło jest za krótkie")]
        public string Password { get; set; }

        [Required]
        [NotMapped]
        [Compare("Password")]
        [Display(Name = "Potwierdź Hasło")]
        [RegularExpression(@"^[a-zA-Z0-9_''-'\s]{8,40}$", ErrorMessage = "Podane znaki są niedozwolone lub hasło jest za krótkie")]
        public string ComparePassword { get; set; }

        [Required]
        [EmailAddress]
        [Key]
        public string Email { get; set; }
        
    }






}
