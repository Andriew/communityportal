﻿namespace PortalSpolecznosciowy.Models
{
    public class EditArticle
    {
        public int ID { get; set; }
        public int Users_ID { get; set; }
        public string Content { get; set; }
        public System.DateTime Date { get; set; }
        public int Accessible { get; set; }
        public bool Image { get; set; }
        public string ImagePath { get; set; }
    }
}
