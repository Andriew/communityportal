﻿using System.Collections.Generic;
using System.Web;
using PortalSpolecznosciowy.Repository;


namespace PortalSpolecznosciowy.Models
{
    public class ArticleAndComments
    {
        public Articles Article { set; get; }
        public IEnumerable<Comments> Comments { get; set; }
        public string UserLogin { get; set; }

        public string CommentContent { get; set; }

        public HttpPostedFileBase File { get; set; }

        public int ArticleID { get; set; }
    }
}
