﻿using System.ComponentModel.DataAnnotations;

namespace PortalSpolecznosciowy.Models
{
    public class ForgottenPassword
    {
        [EmailAddress]
        [Required]
        public string Email { set; get; }
    }
}
