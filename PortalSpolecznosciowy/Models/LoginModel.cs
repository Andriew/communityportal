﻿using System.ComponentModel.DataAnnotations;

namespace PortalSpolecznosciowy.Models
{
    public class LoginModel
    {
        [Display(Name = "Login")]
        [Required (ErrorMessage="Pole Login nie może być puste")]
        [RegularExpression(@"^[a-zA-Z0-9_''-'\s]{3,40}$", ErrorMessage = "Podane znaki są niedozwolone")]
        public string Login { get; set; }
        
        [Display(Name = "Hasło")]
        [DataType(DataType.Password)]
        [RegularExpression(@"^[a-zA-Z0-9_''-'\s]{3,40}$", ErrorMessage = "Podane znaki są niedozwolone")]
        public string Password { get; set; }
        
        public string PassTokenHash { get; set; }

        public string Ip { get; set; }

    }
}
