﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using PortalSpolecznosciowy.Repository;

namespace PortalSpolecznosciowy.Models
{
    public class AddArticle
    {
        [Required]
        public string UserLogin { get; set; }

        public string ArticleContent { get; set; }

        [FileExtensions(Extensions = "jpg,jpeg,png", ErrorMessage = "To nie jest plik z obrazem.")]
        public HttpPostedFileBase File { get; set; }
    }
}
