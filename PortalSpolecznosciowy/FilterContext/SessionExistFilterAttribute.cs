﻿using System;
using System.Web.Mvc;

namespace PortalSpolecznosciowy.FilterContext
{
    class SessionExistFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                var userSession = filterContext.HttpContext.Session["Login"];
                if (userSession == null)
                {
                    filterContext.Result = new RedirectResult("~/");
                }
            }
            catch (Exception)
            {
                filterContext.Result = new RedirectResult("~/");
            }
            base.OnActionExecuting(filterContext);
        }
    }
}
