﻿using System.Net;
using System.Net.Mail;
using PortalSpolecznosciowy.Repository;
using SendGrid;

namespace PortalSpolecznosciowy.FuncHelpers
{
    public class SendEmail
    {
        private Web transportWeb;

        public SendEmail()
        {
            string apikey = "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";
            string name = "andrie";
            string password = "AndrieW01";

            var credentials = new NetworkCredential(name, password);
            transportWeb = new Web(credentials);
            transportWeb = new Web(apikey);
        }

        public void SendForRegister(Users user, string server)
        {
            // Create the email object first, then add the properties.
            var myMessage = new SendGridMessage();

            // Add the message properties.
            myMessage.From = new MailAddress("admin@portalspolecznosciowy.com");

            myMessage.AddTo(user.Login + " <" + user.Email + ">");
            myMessage.Subject = "Witaj w portalu społecznościowym!";

            //Add the HTML and Text bodies
            myMessage.Html = "<p>Witaj! "+ user.Login + "</p>" +
                             "<p>Zarejestrowałeś się na naszym portalu społecznościowym.<p>" +
                             "<p>Aby móc się zalogować kliknij w poniższy link aktywacyjny:<p>" +
                             "<p>" + server + "/RegisterUser/Activation?user=" + user.Login +"&token="+ user.Token +" </p>";
            
            // Send the email, which returns an awaitable task.
            transportWeb.DeliverAsync(myMessage);

            // If developing a Console Application, use the following
            // transportWeb.DeliverAsync(mail).Wait();

        }

        public void SendForForgottenPassword(Users user, string server)
        {
            // Create the email object first, then add the properties.
            var myMessage = new SendGridMessage();

            // Add the message properties.
            myMessage.From = new MailAddress("admin@portalspolecznosciowy.com");

            myMessage.AddTo(user.Login + " <" + user.Email + ">");
            myMessage.Subject = "Zmiana hasła";

            //Add the HTML and Text bodies
            myMessage.Html = "<p>Witaj! " + user.Login + "</p>" +
                             "<p>Link do zresetowania hasła: <p>" +
                             "<p>" + server + "/ForgottenPassword/Activation?user=" + user.Login + "&token=" + user.Token + " </p>";
            
            // Send the email, which returns an awaitable task.
            transportWeb.DeliverAsync(myMessage);

            // If developing a Console Application, use the following
            // transportWeb.DeliverAsync(mail).Wait();

        }

        public void SendForNewEmail(Users user, string email, string server)
        {
            // Create the email object first, then add the properties.
            var myMessage = new SendGridMessage();

            // Add the message properties.
            myMessage.From = new MailAddress("admin@portalspolecznosciowy.com");

            myMessage.AddTo(user.Login + " <" + email + ">");
            myMessage.Subject = "Zmiana adresu email";

            //Add the HTML and Text bodies
            myMessage.Html = "<p>Witaj! " + user.Login + "</p>" +
                             "<p>Link do aktywacji nowego adresu email: <p>" +
                             "<p>" + server + "/Login/ChangeEmailRequest?user=" + user.Login + "&token=" + user.Token + "&email=" + email + "</p>";

            // Send the email, which returns an awaitable task.
            transportWeb.DeliverAsync(myMessage);

            // If developing a Console Application, use the following
            // transportWeb.DeliverAsync(mail).Wait();

        }

    }
}
